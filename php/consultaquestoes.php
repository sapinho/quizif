<?php
ini_set("display_errors", 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


echo '<pre>Inicio da script PHP</pre>';

//Definindo conexão com o banco de dados
$conn = mysqli_connect("localhost", "root", "ifsp", "quizif");

//Vereficando se a conexão deu certo; Erros de conexão: user e password errado
if (!$conn) {
    die("Conexão falhou: " . mysqli_connect_error());
}
echo "Conexão feita com sucesso";



$query = "select * from questoes;"; //Mesmo uso que no banco de dados
$results = mysqli_query($conn, $query);
$index = 0;


while ($record = mysqli_fetch_row($results)) { //Enquanto existir uma linha no banco de dados, o while continuará rodando, e separa a linha em questão e joga na variável record
    $question = array(
        'id' => $record[0],    //->Joga a variavel id na posição 0 da matriz, a qual corresponde
        'titulo' => $record[1], //->Joga a variavel titulo na posição 1 da matriz, a qual corresponde
        'descricao' => $record[2], //->Joga a variavel descrição na posição 2 da matriz, a qual corresponde

        //Atividade aula
        'disciplina' => $record[3],
        'dificuldade' => $record[4]
        //reconstruindo do jeito em que está no banco de dados
    );
    $questions[$index] = $question;
    $index++;
}


// Close the connection
mysqli_close($conn);
$formattedData =  json_encode($questions, JSON_PRETTY_PRINT);
echo "<pre>" . $formattedData . "</pre>";

// header("Location: /index.html");
// exit();

?>
